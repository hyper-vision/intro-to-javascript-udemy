const path = require("path");

module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "dist/assets"),
    filename: "bundle.js"
  },
  devServer: {
    contentBase: path.resolve(__dirname, "dist"),
    publicPath: "/assets/"
  },
  module: {
    rules: [
      {
        // Testing for .js files
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      },
      {
        // Testing for .css files
        test: /\.css$/,
        // Style loader adds css to html
        use: ["style-loader", "css-loader"]
      }
    ]
  }
};
