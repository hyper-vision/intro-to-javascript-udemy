// const userOne = {
//   username: "ryu",
//   email: "ryu@gmail.com",
//   login() {
//     console.log("the user logged in");
//   },
//   logout() {
//     console.log("the user logged out");
//   }
// };

// console.log(userOne.email, userOne.username);
// userOne.login();

// const userTwo = {
//   username: "chun-li",
//   email: "chun.li@gmail.com",
//   login() {
//     console.log("the user logged in");
//   },
//   logout() {
//     console.log("the user logged out");
//   }
// };

// console.log(userTwo.email, userTwo.username);
// userTwo.login();

// Above is inefficient, we should use classes instead.
class User {
  constructor(name, email) {
    // set up properties
    this.username = name;
    this.email = email;
    this.score = 0;
  }

  login() {
    console.log(`${this.username} just logged in.`);
    return this;
  }

  logout() {
    console.log(`${this.username} just logged out.`);
    return this;
  }
  incScore() {
    this.score += 1;
    console.log(`${this.username} has a score of ${this.score}`);
    return this;
  }
}

const userOne = new User("mario", "koopastomper@gmail.com");
const userTwo = new User("luigi", "ghostcleaner@gmail.com");

// Method chaining: to chain methods together, the methods must return
// the this object.
userOne
  .login()
  .incScore()
  .incScore()
  .logout();

console.log(userOne, userTwo);

// the 'new' keyword
// 1 - it creates a new empty object {}
// 2 - it binds the value of 'this' to the new empty object
// 3 - it calls the constructor function to 'build' the object
// this - refers to the current object/class instance

// Class Inheritance
class Admin extends User {
  // Super() allows us to get access to data
  // defined in the parent constructor.
  // We must call Super() before we can use 'this' in
  // the derived class.
  constructor(username, email, title) {
    super(username, email);
    this.title = title;
  }

  deleteUser(user) {
    users = users.filter(u => {
      // Filters out the true one
      return u.username !== user.username;
    });
  }
}

const admin = new Admin("Red", "red@gmail.com", "Administrator");
// console.log(admin);

// let users = [userOne, userTwo, admin];
// console.log(users);

// admin.deleteUser(userTwo);
console.log(userOne, userTwo, admin);
AdmAdminin;
