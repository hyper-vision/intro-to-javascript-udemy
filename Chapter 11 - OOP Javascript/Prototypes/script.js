// Defining objects using the ES5 method of
// prototypes.

/* The class keyword in ES6 is simply an abstraction of prototypes. JS does not have an actual class concept.
Here we can see how 'classes' were implemented in ES5. The function User acts as the constructor.
Inside it we can use 'this' to define variables and methods. */

function User(username, email) {
  this.username = username;
  this.email = email;
}

/* However, defining methods directly inside the constructor
 is not ideal. It is better to define it inside a prototype.
 All objects in JS have a prototype, we can add out method to the User
 prototype like so.*/

User.prototype.login = function() {
  console.log(`${this.username} has logged in`);
  return this;
};

User.prototype.logout = function() {
  console.log(`${this.username} has logged out`);
  return this;
};

// Prototypal Inheritance
function Admin(username, email, title) {
  // Insted of Super() we use the ParentClass.call() method
  // The call method takes 'this' first to specify which object
  // it is referencing, then the parameters of the parent constructor.
  User.call(this, username, email);
  this.title = title;
}

// Currently Admin only has access to the variables in User,
// but not the methods. To have access to the methods we need
// to add the User prototype to the Admin prototype.
// Basically we are creating an instance of the User prototype inside
// the Admin prototype. (This becomes a nested prototype inside
// the Admin prototype)
Admin.prototype = Object.create(User.prototype);

// If we add another function to the Admin prototype then it will
// show up directly under the __proto__ object.
Admin.prototype.deleteUser = function() {
  // delete a user
};

const userOne = new User("mario", "koopacrusher@gmail.com");
const userTwo = new User("luigi", "ghostbuster@gmail.com");

const admin = new Admin("red", "red@gmail.com", "Head Ninja");

console.log(userOne, userTwo);
userOne.login().logout();
console.log(admin);
