/*When listening for a 'submit' event, we attach the event
 * listener to the 'form' itself. Not the submit button.*/

const form = document.querySelector(".signup-form");
//const username = document.querySelector('#username');

// form.addEventListener("submit", event => {
//   /*Default action of submit event is to refresh
// 	page. We want to do something else, so we use
// 	preventDefault to prevent page refresh*/
//   event.preventDefault();
//   /*Instead of creating a reference to the input field
// 	we can access it from the form reference itself using
// 	dot notation. Usage: form.id_of_field.property*/
//   console.log(form.username.value);
// });

//testing RegEx

// const username = "lingling";
// const pattern = /[a-z]{6,}/; //a-z, >=6 char long

// // let result = pattern.test(username);
// // console.log(result);

// // if (result) {
// //   console.log("Regex Passed!");
// // } else {
// //   console.log("Regex Failed!");
// // }

// // search is a string method that takes a regex pattern
// // and searches the string with that pattern.
// // If it finds a match it returns index of matched
// // portion, else -1
// let result = username.search(pattern);
// console.log(result);

const feedback = document.querySelector(".feedback");

form.addEventListener("submit", event => {
  event.preventDefault();

  // validation
  // get and store username
  const username = form.username.value;
  // const usernamePattern = /^[a-zA-Z]{6,12}$/;

  // feedback help info
  // if (usernamePattern.test(username)) {
  //   feedback.style.display = "block";
  //   feedback.className = "feedback feedback-success";
  //   feedback.textContent = "That Username is Valid!";
  // } else {
  //   feedback.style.display = "block";
  //   feedback.className = "feedback feedback-error";
  //   feedback.textContent =
  //     "Username must contain letters only and be between 6-12 characters long.";
  // }
});

// Live Feedback (Keyboard Events)
form.username.addEventListener("keyup", event => {
  const usernamePattern = /^[a-zA-Z]{6,12}$/;

  // console.log(event.target.value, form.username.value);
  if (usernamePattern.test(event.target.value)) {
    feedback.style.display = "block";
    feedback.className = "feedback feedback-success";
    feedback.textContent = "That Username is Valid!";
  } else {
    feedback.style.display = "block";
    feedback.className = "feedback feedback-error";
    feedback.textContent =
      "Username must contain letters only and be between 6-12 characters long.";
  }
});
