// Conditionals
// If Statements
const age = 20;

if (age > 20) {
  console.log("Over 20");
}

const ninjas = ["red", "ryu", "chun-li", "yoshi"];

if (ninjas.length > 3) {
  console.log("That's a lot of ninjas");
}
