const names = ["red", "mario", "luigi"];
let i = 0;

// For Loops
// for (let i = 0; i < 5; i++) {
//   console.log("in loop: ", i);
// }

// console.log("loop finished");

// Looping through items in a list

// const names = ["red", "mario", "luigi"];

// for (let i = 0; i < names.length; i++) {
//   //   console.log(i, names[i]);
//   let html = `<div>${names[i]}</div>`;
//   console.log(html);
// }

// While Loops

// let i = 0;
// while (i < 5) {
//   console.log(i);
//   i++;
// }

// while (i < names.length) {
//   console.log(i, names[i]);
//   i++;
// }

// Do While loop
// Run atleast once

do {
  console.log(i);
  i++;
} while (i < 5);
