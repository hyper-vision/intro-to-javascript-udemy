// Object Literals
// Similar to Python Dictionaries

// let user = {
//   name: "Crystal",
//   age: 30,
//   email: "crysta@gmail.com",
//   location: "Berlin",
//   blogs: ["Why Me", "10 Things to Make With Vegimite"]
// };

// Accessing properties of objects (dot notation)
// console.log(user);
// console.log(user.name);

// Changing values of properties
// user.age = 35;
// console.log(user.age);

// Accessing properties of objects (square bracket notation)
// console.log(user["name"]);
// user["name"] = "Guile";

// console.log(user["name"]);

/*  
    Both methods have their uses. Dot notation
    is usually more convenient but square
    bracket notation is useful in situations such
    as the following:
*/

// let key = "location";
// console.log(user[key]);

// Here user.key would not work since user
// has no property called 'key'.

// Adding Methods to Objects
// Methods = functions defined in objects

// let user = {
//   name: "Crystal",
//   age: 30,
//   email: "crysta@gmail.com",
//   location: "Berlin",
//   blogs: ["Why Me", "10 Things to Make With Vegemite"],

//   login: function() {
//     console.log("The user logged in!");
//   },
//   // Shorthand for functions in objects
//   // we can omit the 'function' keyword.
//   logout() {
//     console.log("The user logged out!");
//   },
//   logBlogs() {
//     console.log(this.blogs);
//   }
// };

// user.login();
// user.logout();
// user.logBlogs();
// console.log(user);

/* 
We use regular functions and not arrow functions
within objects because when we use
an arrow function, the scope of 'this' is 
set to the scope of the statement which 
CALLS the function.

With regular functions, the scope of 'this' is
set to the scope of the statement which 
DECLARES the function.
*/

// Objects in Arrays

// let user = {
//   name: "Crystal",
//   age: 30,
//   email: "crysta@gmail.com",
//   location: "Berlin",
//   blogs: [
//     { title: "Why Me", likes: 30 },
//     { title: "10 Things To Make With Vegemite", likes: 30 }
//   ],

//   login: function() {
//     console.log("The user logged in!");
//   },
//   // Shorthand for functions in objects
//   // we can omit the 'function' keyword.
//   logout() {
//     console.log("The user logged out!");
//   },
//   logBlogs() {
//     this.blogs.forEach(blog => {
//       console.log(blog);
//     });
//   }
// };

// user.logBlogs();

// Math Object

// console.log(Math);
// console.log(Math.PI);
// console.log(Math.E);

// const area = 7.7;

// console.log(Math.round(area));
// console.log(Math.floor(area));
// console.log(Math.ceil(area));
// console.log(Math.trunc(area));

// // Random Numbers
// const random = Math.random();

// console.log(random);
// console.log(Math.round(random * 100));

// Primitive vs Reference Types
/* 
  Primitive: numbers, strings, Booleans, null, undefined, symbols
  Reference: all types of objects: obj literals, arrays, funcations, dates, etc. (e.g. Math)

  Primitive: Stored on the Stack - limited memory - faster
  Referebce: Stored on the Heap - more memory - slower
*/

// Primitive Values

let scoreOne = 50;
let scoreTwo = scoreOne;

console.log(`Score 1: ${scoreOne}, Score 2: ${scoreTwo}`);

scoreTwo = 100;

console.log(`Score 1: ${scoreOne}, Score 2: ${scoreTwo}`);

// Reference Types

const userOne = { name: "ryu", age: 30 };
const userTwo = userOne;

console.log(userOne, userTwo);

userOne.age = 40;

console.log(userOne, userTwo);
