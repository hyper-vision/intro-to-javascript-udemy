// render chat templates to the DOM
// clear the list of chats (when the room changes)

class ChatUI {
  constructor(list) {
    // Reference to the chat list on our HTML document
    this.list = list;
  }

  clear() {
    this.list.innerHTML = "";
  }

  render(data) {
    //   Using datefns to return how old a message is
    const when = dateFns.distanceInWordsToNow(data.created_at.toDate(), {
      addSuffix: true //Adds the word 'ago'
    });
    const html = `
      <li class = "list-group-item">
        <span class="username">${data.username}</span>
        <span class="message">${data.message}</span>
        <div class="time">${when}</div>
      </li>
      `;

    this.list.innerHTML += html;
  }
}
