// adding new chat documents
// setting up a real-time listener to get new chats
// updating the username
// updating the room

class Chatroom {
  constructor(room, username) {
    this.room = room;
    this.username = username;
    this.chats = db.collection("chats"); // Reference to chat collection
    this.unsub; // We'll use this to unsub from listening to a chatroom when the user changes rooms
  }
  //   Add a message document to chats
  async addChat(message) {
    // format a chat object
    const now = new Date();
    const chat = {
      // same key and value so no need for naming it twice
      message,
      username: this.username,
      room: this.room,
      created_at: firebase.firestore.Timestamp.fromDate(now)
    };
    // Save chat doc to database
    const response = await this.chats.add(chat);
    return response;
  }
  //   Realtime listener for changes
  getChats(callback) {
    // Where allows us to sort documents according to some condition
    // orderBy allows for ordering by a field
    this.unsub = this.chats
      .where("room", "==", this.room)
      .orderBy("created_at")
      .onSnapshot(snapshot => {
        snapshot.docChanges().forEach(change => {
          if (change.type === "added") {
            // Update UI
            callback(change.doc.data());
          }
        });
      });
  }

  updateName(username) {
    this.username = username;
    // store updated name in local storage
    localStorage.setItem("username", username);
  }
  updateRoom(room) {
    this.room = room;
    console.log("room updated to", this.room);
    if (this.unsub) {
      this.unsub(); // if subscribed to a room then unsubscribe from that room
    }
  }
}

/* After unsubbing from the old room we need to create a new
listener with our new room.
*/

// setTimeout(() => {
//   chatroom.updateRoom("gaming");
//   chatroom.updateName("yoshi");

//   chatroom.getChats(data => {
//     console.log(data);
//   });

//   chatroom.addChat("hello");
// }, 3000);
