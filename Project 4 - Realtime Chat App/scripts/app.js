// DOM queries
const chatList = document.querySelector(".chat-list");
const newChatForm = document.querySelector(".new-chat");
const newNameForm = document.querySelector(".new-name");
const updateMsg = document.querySelector(".update-msg");
const rooms = document.querySelector(".chat-rooms");

// add a new chat
newChatForm.addEventListener("submit", event => {
  event.preventDefault();

  const message = newChatForm.message.value.trim();
  console.log(message);
  chatroom
    .addChat(message)
    .then(() => newChatForm.reset())
    .catch(err => console.log(err));
});

// update username
newNameForm.addEventListener("submit", e => {
  e.preventDefault();
  // update name via chatroom class
  const newName = newNameForm.name.value.trim();
  chatroom.updateName(newName);
  // reset form
  newNameForm.reset();
  // show then hide the update message
  updateMsg.innerText = `Your name was updated to ${newName}`;
  newChatForm.message.setAttribute("placeholder", `Post as ${newName}`);
  document.title = newName;
  setTimeout(() => (updateMsg.innerText = ""), 3000);
});

// update chatroom
rooms.addEventListener("click", event => {
  if (event.target.tagName === "BUTTON") {
    chatUI.clear();
    chatroom.updateRoom(event.target.getAttribute("id"));
    chatroom.getChats(chat => chatUI.render(chat));
  }
});

// Check local storage for username
const username = localStorage.username ? localStorage.username : "anon";

// Class instances
const chatUI = new ChatUI(chatList);

// creating the first instance of chatroom
const chatroom = new Chatroom("general", username);
newChatForm.message.setAttribute("placeholder", `Post as ${username}`);

document.title = username;
// get chats and render for the first time
chatroom.getChats(data => chatUI.render(data));
