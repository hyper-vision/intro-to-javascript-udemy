// Get gets us our documents in our collections. This is an async task
// hence it returns a promise.

const list = document.querySelector("ul");
const form = document.querySelector("form");

// Update page
// Add item to DOM
const addRecipe = (recipe, id) => {
  let time = recipe.created_at.toDate();
  //   We will use the id to identify each list item uniquely so we can delete them.
  let html = `
    <li data-id = "${id}">
        <div>${recipe.title}</div>
        <div>${time}</div>
        <button class="btn btn-danger btn-sm my-2">Delete</button>
    </li>
    `;

  list.innerHTML += html;
};

// Delete item from DOM
const deleteRecipe = id => {
  const recipes = document.querySelectorAll("li");
  recipes.forEach(recipe => {
    if (recipe.getAttribute("data-id") === id) {
      recipe.remove();
    }
  });
};

// Get documents
// db.collection("recipes")
//   .get()
//   .then(snapshot => {
//     snapshot.docs.forEach(doc => {
//       //   console.log(doc.id);
//       addRecipe(doc.data(), doc.id);
//     });
//   })
//   .catch(err => {
//     console.log(err);
//   });

// We are going to use realtime listeners to check the db for changes every time something
// changes on the page. So whenever something changes on the backend we will update the page.
/* When any change happens on the db, firebase takes a snapshot of the new state. the onSnapshot
listener listens for whenever firebase makes a snapshot and executes some logic when it happens. */
db.collection("recipes").onSnapshot(snapshot => {
  snapshot.docChanges().forEach(change => {
    const doc = change.doc;
    if (change.type === "added") {
      addRecipe(doc.data(), doc.id);
    } else if (change.type === "removed") {
      deleteRecipe(doc.id);
    }
  });
});

// add documents (items to the list)
form.addEventListener("submit", event => {
  event.preventDefault();

  const now = new Date(); // Date added
  const recipe = {
    title: form.recipe.value,
    created_at: firebase.firestore.Timestamp.fromDate(now) // creates a firestore compatible timestamp
  };

  db.collection("recipes")
    .add(recipe)
    .then(() => {
      console.log("recipe added");
    })
    .catch(err => {
      console.log(err);
    });
});

// Deleting data (using document id provided by firestore)
list.addEventListener("click", event => {
  if (event.target.tagName === "BUTTON") {
    const id = event.target.parentElement.getAttribute("data-id");
    db.collection("recipes")
      .doc(id)
      .delete()
      .then(() => {
        console.log("recipe deleted");
      })
      .catch(err => {
        console.log(err);
      });
  }
});
