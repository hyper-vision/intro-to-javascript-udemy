//Strings

console.log("hello world");
let email = "mario@mushroom.com";

console.log(email);

// Concatenation
let firstname = "Geraldo";
let lastname = "del Riviera";

let fullname = firstname + " " + lastname;

console.log(fullname);

// Getting chars from string
console.log(fullname[2]);

// String length
console.log(fullname.length);

// String methods
console.log(fullname.toUpperCase());
let result = fullname.toLowerCase();

console.log(result);

// Original variable is unchanged
console.log(fullname);

// Getting index of char from string
let index = email.indexOf("@");
console.log(index);
