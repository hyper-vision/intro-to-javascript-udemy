// Common String Methods
let email = "mario@mushroom.com";

// lastIndexOf() Returns the index of last
// occurance of a char in a string.
// let result = email.lastIndexOf("m");

// slicing, slice(from, to)
// slice(2,10) returns 2nd-10th elements
// Returns a portion of the string.
// let result = email.slice(0, 5);

// Substring, substr(from, offset)
// substr(2, 10) returns 2nd-12th elements
// let result = email.substr(2, 10);

// Replace, replace(what, with what)
// Finds first instance and replaces
let result = email.replace("m", "w");

console.log(result);
