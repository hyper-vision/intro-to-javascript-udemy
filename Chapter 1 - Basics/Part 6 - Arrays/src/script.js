// Arrays (object datatype)
let ninjas = ["red", "ryu", "ken"];

console.log(ninjas);
console.log(ninjas[1]);

// Overwrite
ninjas[1] = "chun-li";

console.log(ninjas);

// Number Arrays
let ages = [20, 25, 30, 35];
console.log(ages[2]);

// Mixed data types
let random = ["red", "egg", 39, 20];
console.log(random);

// length property
console.log(ninjas.length);

// Array methods
// Join - Joins all elements in array into string
// using some delimiter
// let result = ninjas.join(",");

// indexOf - Return index of an element
// let result = ninjas.indexOf("ken");

// Concat - join arrays
// let result = ninjas.concat(["guile"]);

// Push - Pushes value into array and returns new length
// This method is known as a 'destructive method'
// since it changes the value
let result = ninjas.push("e-honda");

// Pop - Removes last value in array and returns popped value

result = ninjas.pop();
console.log(ninjas);
console.log(result);
