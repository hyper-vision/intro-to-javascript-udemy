// Variables
let age = 29;
let year = 2019;

console.log(age, year);
age = 30;
console.log(age);

// Constants
const points = 100;

console.log(points);

// var
var score = 75;
console.log("Score: ", score);
