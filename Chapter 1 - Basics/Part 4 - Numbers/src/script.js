let radius = 10;
const pi = 3.14;

// Both decimans and ints are considered
// 'numbers' in JS

// console.log(radius, pi);

// Math Operations + - * / ** %

// console.log(10 / 2);
// let result = radius % 3;

// let result = radius * pi ** 2;

// Order of operations - P E D M A S

// let result = 5 * (10 - 3) ** 2;

// console.log(result);

let likes = 10;
// Incrementation & Decrementation
// likes--;

// Operations by n

// likes += 10;

likes -= 7;

// likes /= 5;

likes *= 2;

console.log(likes);

// NaNs - Not a Number
console.log(5 / "hello");
console.log(5 * "hello");

// Concatenation strings with numbers:
let result = "the blog has " + likes + " likes";
console.log(result);
