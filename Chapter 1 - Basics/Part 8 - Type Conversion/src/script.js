// Type Conversion
// Converting one d-type to another
let score = "100";
console.log(score + 1, typeof score);

score = Number(score);
console.log(score + 1, typeof score);

// let result = Number("hello"); //NAN

// let result = String(50); // String

// let result = Boolean(100); // True
// +ve and -ve numbers are considered 'truthy'
// values while 0 is considered a 'falsy' value.

let result = Boolean("0");
// Strings of len > 0 = truthy
// Empty strings = falsy

console.log(result, typeof result);
