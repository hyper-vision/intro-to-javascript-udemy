// Booleans & Comparison
// console.log(true, false);

// Methods can return booleans
let email = "luigi@thenetninja.co.uk";
let names = ["mario", "luigi", "toad"];

// includes - checks whether a val is in string/array
// returns boolean
// let result = email.includes("!");
let result = names.includes("mario");

// console.log(result);

// Comparison Operators
let age = 25;

// console.log(age == 25);
// console.log(age == 50);
// console.log(age != 50);
// console.log(age != 25);
// console.log(age > 20);
// console.log(age < 20);
// console.log(age <= 25);
// console.log(age >= 25);

let name = "Red";

// console.log(name == "Red"); // T
// console.log(name == "red"); // F
// console.log(name > "Crystal"); // R > C
// console.log(name > "red"); // R < r
// console.log(name == "Red");
// console.log(name == "crystal"); // R < c

//  Loose vs Strict Comparisons
// Loose == (compares only values)
// console.log(age == 25);
// console.log(age == "25"); // Implicit conversion

// Strict === (compares values and data-types)
console.log(age === 25);
console.log(age === "25");
console.log(age !== 25);
console.log(age !== "25");
