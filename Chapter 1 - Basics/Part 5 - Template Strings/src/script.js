// Template Strings (like F-Strings in Python)
const title = "Best reads of 2019";
const author = "Mario";
const likes = 30;

// Concatenation way
// let result =
//   "The blog called " + title + " by " + author + " has " + likes + " likes.";
// console.log(result);

// Template string way (backticks and $)
let result = `The blog called "${title}" by ${author} has ${likes} likes.`;
console.log(result);

// Creating HTML templates
let html = `
    <h2>${title}</h2>
    <p>By ${author}</p>

    <span>This blog has ${likes} likes</span>
`;

console.log(html);
