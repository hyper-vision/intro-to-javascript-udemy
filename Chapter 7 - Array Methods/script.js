// Filter - Cycles through each function in the array and compares to a condition.
// Returns boolean
// Non-destructive
// const scores = [10, 30, 15, 25, 50, 40, 5];

// const filteredScores = scores.filter(score => {
//   return score > 20;
// });

// console.log(scores);
// console.log(filteredScores);

// const users = [
//   { name: "Shahun", premium: true },
//   { name: "Yoshi", premium: false },
//   { name: "Mario", premium: false },
//   { name: "Chun-li", premium: true }
// ];

// const premiumUsers = users.filter(user => user.premium); // Returns only if true

// console.log(premiumUsers);

// Map - Iterates over an array and applies a function to
// each element then returns an array with modified elements.
// Non-destructive

// const prices = [20, 10, 30, 15, 25, 50, 40, 80, 5];

// const salesPrice = prices.map(price => price / 2);
// console.log(salesPrice);

// const products = [
//   { name: "Gold Star", price: 20 },
//   { name: "Mushroom", price: 40 },
//   { name: "Green Shells", price: 30 },
//   { name: "Banana Skin", price: 10 },
//   { name: "Red Shells", price: 50 }
// ];

// const newProducts = products.map(product => {
//   if (product.price > 30) {
//     return { name: product.name, price: product.price / 2 };
//   } else {
//     return product;
//   }
// });

// console.log(newProducts);

// Reduce - Iterates over array and returns count of how many values
// meet condition
// Non-destructive

// Callback function takes 2 arguments, accumilator and current_value
// Accumilator remembers value across iterations, used for keeping count
// Reduce() takes 2 arguments, callback_function, initial value of accumilator

// const scores = [10, 30, 15, 25, 50, 40, 5];

// const over20 = scores.reduce((acc, score) => {
//   if (score > 20) {
//     acc++;
//   }
//   return acc;
// }, 0);

// console.log(over20);

// const scores = [
//   { name: "Shahun", score: 20 },
//   { name: "Yoshi", score: 40 },
//   { name: "Mario", score: 54 },
//   { name: "Chun-li", score: 60 },
//   { name: "Shahun", score: 20 },
//   { name: "Yoshi", score: 40 },
//   { name: "Mario", score: 54 },
//   { name: "Chun-li", score: 60 },
//   { name: "Shahun", score: 20 },
//   { name: "Yoshi", score: 40 },
//   { name: "Mario", score: 54 },
//   { name: "Chun-li", score: 60 }
// ];

// const marioTotal = scores.reduce((acc, score) => {
//   if (score.name === "Shahun") {
//     acc += score.score;
//   }
//   return acc;
// }, 0);

// console.log(marioTotal);

// Find - Iterates, returns first val passing condition
// Non-destructive

// const scores = [10, 5, 0, 40, 60, 10, 20, 70];

// const firstHighScore = scores.find(score => {
//   return score > 50;
// });

// console.log(firstHighScore);

// Sort
// DESTRUCTIVE!!!!!!!!!!!!!!!!

// const names = ["mario", "shaun", "chun-li", "yoshi", "luigi"];
// // names.sort();
// names.reverse(); // Reverses array.
// console.log(names);

// const scores = [10, 50, 20, 5, 35, 70, 45];
// scores.sort();
// // Returns - [10, 20, 35, 45, 5, 50, 70] which is not properly sorted (5)
// console.log(scores);

// properly sorting
const players = [
  { name: "Luigi", score: 40 },
  { name: "Yoshi", score: 20 },
  { name: "Mario", score: 54 },
  { name: "Chun-li", score: 60 },
  { name: "Shahun", score: 20 }
];

// For more complex data types like array of objects, the sort
// method accepts a function that takes 2 variables.
// a and b represent pairs of values in the array.
// if a > b => -1, if b > a => 1, if a = b => 0

// players.sort((a, b) => {
//   if (a.score > b.score) {
//     return -1;
//   } else if (a.score < b.score) {
//     return 1;
//   } else {
//     return 0;
//   }
// });

// shorter version
// a - b = ascending
// b - a = descending
// players.sort((a, b) => b.score - a.score);
// console.log(players);

// const scores = [10, 5, 0, 40, 60, 10, 20, 70];
// scores.sort((a, b) => b - a);
// console.log(scores);

// Array Method Chaining
const winners = players
  .filter(player => player.score > 30)
  .map(player => `${player.name}'s is a winner with score ${player.score}`);

console.log(winners);
