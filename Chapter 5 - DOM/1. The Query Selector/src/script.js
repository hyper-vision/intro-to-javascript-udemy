// Accessing elements in the DOM

// Accessing a p tag
// grabs the first p tag encountered
const para = document.querySelector("p");

console.log(para);

// Accessing by class
// Accesses first element with matching name
const errorPara = document.querySelector(".error");
console.log(errorPara);

// Accessing specific classes by tag
const divError = document.querySelector("div.error");
console.log(divError);

// Accessing ALL tags of same type
// Returns a 'nodelist' type. Similar to array,
// but doesn't have same methods.

const allP = document.querySelectorAll("p");
console.log(allP);
// Access by Index
console.log(allP[0]);
// Foreach also works
allP.forEach(para => {
  console.log(para);
});
