// Adding events to elements

// const button = document.querySelector("button");

// // using the addEventListener method
// // 2 arguments ('event', callback-function)
// button.addEventListener("click", () => {
//   console.log("You clicked me!");
// });

const items = document.querySelectorAll("li");

// the callback function in addEventListener returns a
// variable (here called 'event') which contains info
// about the event that occured.
// Contains info such as which element in the html was clicked
// We can use this to delete said element when clicked.
// items.forEach(item => {
//   item.addEventListener("click", event => {
//     // event.target.style.textDecoration = "line-through";
//     event.target.remove();
//     console.log("Event in li");
//     event.stopPropagation();
//   });
// });

// Adding an item when button is clicked
const button = document.querySelector("button");
const ul = document.querySelector("ul");

button.addEventListener("click", event => {
  const li = document.createElement("li");
  li.textContent = prompt("Add an Item");

  ul.prepend(li);
});

/* 
Event bubbling: 

When an event occurs, JS calls the corresponing
callback function if an event listener is attached. Then it checks the
parent element for an event listener with the same event and executes 
it if the parent has one. This bubbles up to the root element. 

IF we don't want events to bubble up then we can use a method on the 
child element called stopPropagation.
*/

// ul.addEventListener("click", event => {
//   console.log("event in ul");
// });

/* 
  Event Deligation:

  Attaching an event listener to each li can be costly if there are many 
  li's. Also, any new li added after the page is loaded will not have
  an event listener attached by default.

  This can affect performance. Instead we can use event deligation, which allows
  us to attach an event listener to the parent element and have that event affect
  all children.
*/

ul.addEventListener("click", event => {
  // console.log(event.target.tagName);
  if (event.target.tagName === "LI") {
    event.target.remove();
  }
});
