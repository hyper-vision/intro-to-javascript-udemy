// Changing text in first paragraph
const para = document.querySelector("p");
// console.log(para.innerText);

// para.innerText = "Ninjas are Awesome";

// Changing text of several items at once
const paras = document.querySelectorAll("p");

// paras.forEach(para => {
//   console.log(para.innerText);
//   para.innerText += " - new text";
// });

// Changing HTML
const content = document.querySelector(".content");

// console.log(content.innerHTML);
// content.innerHTML += "<h2> This is a new H2 </h2>";

// Generating a simple HTML template from a list
const people = ["mario", "luigi", "yoshi"];

people.forEach(person => {
  content.innerHTML += `<p>${person}</p>`;
});
