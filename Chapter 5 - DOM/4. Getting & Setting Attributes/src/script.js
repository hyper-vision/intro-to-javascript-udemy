const link = document.querySelector("a");

// getAttribute allows us to get an
// attribute of an HTML tag

console.log(link.getAttribute("href"));
// setAttribute ("which", "to what");
link.setAttribute("href", "https://www.thenetninja.co.uk");

console.log(link.getAttribute("href"));
link.innerText = "The NetNinja Website";

const msg = document.querySelector("p");

console.log(msg.getAttribute("class"));
msg.setAttribute("class", "success");
console.log(msg.getAttribute("class"));

// Setting attributes to tags that don't
// exist already.
// JS will inject the attribute at runtime
msg.setAttribute("style", "color: green;");
