// How to use the document object model tree structure to
// select sibling and child elements

const article = document.querySelector("article");

// article.children is an HTMLCollection
// which does not have a forEach method
// console.log(article.children);

// // So we must first convert them to an array
// console.log(Array.from(article.children));

// Array.from(article.children).forEach(child => {
//   child.classList.add("article-element");
// });

// Finding an element's parent
const title = document.querySelector("h2");
console.log(title.parentElement);

// Can be chained to find all ancestors
console.log(title.parentElement.parentElement);

// Getting next/previous sibling
console.log(title.nextElementSibling);
console.log(title.previousElementSibling);

// More chaining
console.log(title.nextElementSibling.parentElement.children);
