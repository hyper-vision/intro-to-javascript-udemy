// const content = document.querySelector("p");

// classList returns list of all classes the element
// belongs to.
// console.log(content.classList);

// // Adding classes
// content.classList.add("error");

// // Removing classes
// content.classList.remove("error");

// content.classList.add("success");

const pList = document.querySelectorAll("p");

pList.forEach(p => {
  if (p.textContent.includes("success")) {
    p.classList.add("success");
  } else if (p.textContent.includes("error")) {
    p.classList.add("error");
  }
});

// Third method of classList property: toggle
const title = document.querySelector(".title");

title.classList.toggle("test"); // Adds class test to title
title.classList.toggle("test"); // Removes class test from title
