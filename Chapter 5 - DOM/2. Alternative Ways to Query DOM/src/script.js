// get element by ID
// getElementById grabs only 1
const title = document.getElementById("page-title");
console.log(title);

// get elements by their class name
// getElementsByClassName can grab multiple elements
// Returns HTMLCollection type - similar to nodeList type
// but not identical.
// HTMLCollection does not have a forEach method
const errors = document.getElementsByClassName("error");
console.log(errors);
console.log(errors[0]);

// get element by their tag name
const paras = document.getElementsByTagName("p");
console.log(paras);
console.log(paras[0]);
