// Copy event
const copy = document.querySelector(".copy-me");

copy.addEventListener("copy", () => {
  console.log("Oi!");
});

// Tracking mouse within box
const box = document.querySelector(".box");

box.addEventListener("mousemove", event => {
  //   console.log(event.offsetX, event.offsetY);
  box.textContent = `${event.offsetX}, ${event.offsetY}`;
});

// Adding an event listener directly on the document object
// To track scroll events

document.addEventListener("wheel", event => {
  console.log(event.pageX, event.pageY);
});
