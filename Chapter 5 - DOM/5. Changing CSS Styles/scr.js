const title = document.querySelector('h1');

// using setAttribute('style') will completely overwrite any existing styles
// on the element.

// title.setAttribute('style', 'margin:50px;');

// If we want to add to the exisiting style then we can
// use the style property of the element.
console.log(title.style);
console.log(title.style.color);

// Adding a margin
title.style.margin = '50px';

// Changing the color
title.style.color = 'crimson';

// Changing font-size
// in CSS the property is font-size
// but in JS font-size translates to font - size (subtraction)
// so all hyphenated properties are converte to camelCasing.

title.style.fontSize = '60px';

// Removing a property
// Set the property to an empty string ('')

title.style.margin = '';
