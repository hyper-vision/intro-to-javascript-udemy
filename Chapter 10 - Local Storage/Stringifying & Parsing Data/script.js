// Storing more complex data such as array of objects
// in localStorage
const todos = [
  { text: "play mariokart", author: "shaun" },
  { text: "buy some milk", author: "mario" },
  { text: "buy some bread", author: "luigi" }
];

console.log(JSON.stringify(todos));
// items in localStorage must be strings. stringify
// allows us to convert objects and object arrays to strings.
localStorage.setItem("todos", JSON.stringify(todos));

// Retrieving
// Returns as string, must parse back into JSON
const storedTodos = localStorage.getItem("todos");
console.log(JSON.parse(storedTodos));
