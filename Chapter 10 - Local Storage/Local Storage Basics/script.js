// localStorage is a window property that stores persistent data.
// localStorage data has no expiration date and is available even
// after the browser is closed.

// Store data in local storage
// setItem(key, value)
// localStorage always stores in string
// localStorage.setItem("name", "Mario");
// localStorage.setItem("age", 50);

// get data from local storage
let name = localStorage.getItem("name");
let age = localStorage.getItem("age");

// console.log(name, age);

// // Updating data
// // also using setItem
// // If key exists - update, else - create
// localStorage.setItem("name", "luigi");

// // We can also update items using dot notation
// localStorage.age = 49;

// name = localStorage.getItem("name");
// age = localStorage.getItem("age");
console.log(name, age);

// Deleting items from localStorage
// Single item
localStorage.removeItem("name");
// All items
localStorage.clear();
