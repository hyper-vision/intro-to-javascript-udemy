// Reference to city form
const cityform = document.querySelector("form");
const card = document.querySelector(".card");
const details = document.querySelector(".details");
const timeImage = document.querySelector("img.time");
const weatherIcon = document.querySelector(".icon img");

const forecast = new Forecast();

// Update the UI
const updateUI = data => {
  // console.log(data);
  // const cityDetails = data.cityDetails;
  // const weather = data.weather;

  // Destructuring (easier way to do the above)
  const { cityDetails, weather } = data;

  // Update Details Template
  details.innerHTML = `
  <h5 class="my-3">${cityDetails.EnglishName}</h5>
      <div class="my-3">${weather.WeatherText}</div>
      <div class="display-4 my-4">
    <span>${weather.Temperature.Metric.Value}</span>
    <span>&deg;C</span>
  `;

  // Update the night/day & icon images
  let timeSrc = weather.IsDayTime ? "./img/day.svg" : "./img/night.svg"; // Using ternary operators
  const iconSrc = `img/icons/${weather.WeatherIcon}.svg`;

  timeImage.setAttribute("src", timeSrc);
  weatherIcon.setAttribute("src", iconSrc);

  // Remove d-none class if present
  if (card.classList.contains("d-none")) {
    card.classList.remove("d-none");
  }
};

// Listen for user input
cityform.addEventListener("submit", event => {
  event.preventDefault();

  // Get city value
  const city = cityform.city.value.trim();
  cityform.reset();

  // Update UI with new city
  forecast
    .updateCity(city)
    .then(data => updateUI(data))
    .catch(error => console.log(error));

  // store city in local storage
  localStorage.setItem("city", city);
});

// If city in local storage, then update UI with
// city details, else do nothing.
if (localStorage.getItem("city")) {
  forecast
    .updateCity(localStorage.getItem("city"))
    .then(data => updateUI(data))
    .catch(error => console.log(error));
}
