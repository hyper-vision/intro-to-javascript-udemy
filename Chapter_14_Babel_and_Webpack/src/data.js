/* Default export - the main thing we want to export from a file.
We can only have one default export. */

const users = [
  { name: "mario", premium: true },
  { name: "luigi", premium: false },
  { name: "yoshi", premium: true },
  { name: "toad", premium: true },
  { name: "peach", premium: false }
];

const getPremUsers = users => {
  return users.filter(user => user.premium);
};

export { getPremUsers, users as default };
