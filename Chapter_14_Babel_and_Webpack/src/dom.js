console.log("Dom file");

const body = document.querySelector("body");

/* To make something accessable in another javascript file, we
must first export it using the 'export' keyword.
We can add export to function definition: export const styleBody = ...
Or add it at the end of the document*/

const styleBody = () => {
  body.style.background = "peachpuff";
};

const addTitle = text => {
  const title = document.createElement("h1");
  title.textContent = text;
  body.appendChild(title);
};

const contact = "mario@gmail.com";

export { styleBody, addTitle, contact };
