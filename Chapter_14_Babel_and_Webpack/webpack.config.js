// Importing node module 'path'
// With Node you can't just concatenate with +, hence we need path.resolve()
const path = require("path");

module.exports = {
  entry: "./src/index.js",
  output: {
    //   __dirname - built in node attribute, absolute path to current node directory
    path: path.resolve(__dirname, "dist/assets"),
    filename: "bundle.js"
  },
  devServer: {
    // The directory we want to serve up (abs path)
    contentBase: path.resolve(__dirname, "dist"),
    publicPath: "/assets/"
  },
  module: {
    rules: [
      {
        // Testing for any file ending in .js
        // and excluding anything in the node_modules folder
        // which loader to use
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      }
    ]
  }
};
