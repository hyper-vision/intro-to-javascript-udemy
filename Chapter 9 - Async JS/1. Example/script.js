console.log(1);
console.log(2);

// Something that takes a while to do
setTimeout(() => {
  console.log("The Callback function fired!");
}, 2000);

console.log(3);
console.log(4);

// setTimeout is async code so it will not block
// the succeding console.log() statements
