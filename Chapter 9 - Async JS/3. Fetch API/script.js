// We have been using the XMLHttpRequest module for accessing
// APIs. But there is a newer method using the fetch API.
// Much sipmler and cleaner.

// Fetch returns a promise implicitly
// fetch("./todos/luiji.json")
//   .then(response => {
//     console.log("Resolved!", response);
//     return response.json();
//   })
//   .then(data => {
//     console.log(data);
//   })
//   .catch(error => {
//     console.log("Rejected", error);
//   });

// Fetch's .catch() will not fire if the given address
// is not valid. It will simply return Resolved with a
// 404 status. .catch() only executes if there was some kind of
// network error.

// the response object has a method .json(), that can
// take the response string and parse it into a JS object.
// This method returns a promise (if the JSON string is large it could
// take some time) so we can append a .then() to the end of the parent
// function to recieve the data and use it.

// Async and Await
// Async and await allows us to chain together promises more cleanly
// First, declare an async function (always returns promise)
const getTodos = async () => {
  // Await makes fetch call asynchronous, async keyword above makes getTodos a
  // non-blocking function.
  const response = await fetch("./todos/luijis.json");

  // Throwing our own error method
  // This error is caught by our .catch() method below
  // This allows us to return specific errors for specific cases
  // instead of relying on the built-in error handling, which may
  // return strange confusing error messages unrelated to the main cause.
  if (response.status !== 200) {
    throw new Error("Cannot fetch the data");
  }

  const data = await response.json();

  return data; // any async function returns a promise
  // So we are not actually returning just data
};

console.log(1);
console.log(2);

getTodos()
  .then(data => console.log("Resolved: ", data))
  .catch(error => console.log("Rejected: ", error.message));

console.log(3);
console.log(4);
