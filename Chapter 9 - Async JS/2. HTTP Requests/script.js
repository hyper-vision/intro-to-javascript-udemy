const getTodos = resource => {
  return new Promise((resolve, reject) => {
    // Creating a request (UNSENT - 0)
    const request = new XMLHttpRequest();

    // Tracking the progresss of our request
    // readystatechange fires whenever the request goes through a
    // state change. (Returns a state change code)
    // State chage codes: https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/readyState
    request.addEventListener("readystatechange", () => {
      //   console.log(request, request.readyState);
      if (request.readyState === 4 && request.status === 200) {
        // The responseText is a long string (as strings are how all
        // HTTP communication happens). We need to parse it into JSON
        // to use it.
        // All JSON keys and strings have to be in "", not ''
        const data = JSON.parse(request.responseText);
        resolve(data);
      } else if (request.readyState === 4) {
        reject("Error getting resource!");
      }
    });

    // Open a connection to our endpoint (OPENED - 1)
    request.open("GET", resource);
    // Send request and recieve headers (HEADERS_RECIEVED - 2)
    request.send();

    // LOADING - 3
    // DONE - 4
  });
};

// console.log(1);
// console.log(2);

// If we want to access data IN ORDER of calling, then we can nest function calls.
// This is one (very inefficient) way to achieve this.
// getTodos("./todos/luiji.json", (error, data) => {
//   console.log(data);
//   getTodos("./todos/mario.json", (error, data) => {
//     console.log(data);
//     getTodos("./todos/max.json", (error, data) => {
//       console.log(data);
//     });
//   });
// });

// console.log(3);
// console.log(4);

// A better alternative is using Promises
// Promise example
// const getSomething = () => {
//   // A promise is either resolved or rejected
//   // Promise returns two values, resolve and reject
//   return new Promise((resolve, reject) => {
//     // Fetch something
//     // resolve("Some Data");
//     reject("Some Error");
//   });
// };

// Simply calling the promise method will not execute a promise, it will only return the promise itself. We must use the
// .then() method to actually execute the promise that is returned. The .then method takes two functions as parameters.
// The first is the resolve function which does something the the data passed to it if our request resolved successfully.
// The second is the error function, which does something if our request was uncessful.
// getSomething().then(
//   data => {
//     console.log(data);
//   },
//   error => {
//     console.log(error);
//   }
// );

// Another way to do the same thing as above is to use then/catch. When using catch, then only takes the resolve
// function. Another method called, then, takes the error function. This is (somewhat) less confusing than
// the above way.
// getSomething()
//   .then(data => {
//     console.log(data);
//   })
//   .catch(error => {
//     console.log(error);
//   });

// Using promise to return data from our getTodos function.
// getTodos("./todos/luiji.json")
//   .then(data => {
//     console.log("Promise Resolved: ", data);
//   })
//   .catch(error => {
//     console.log("Promise Rejected: ", error);
//   });

// Chaining promises to return data in sequence
// Chaining is an alternative to nesting callback functions like before.
// Here we can simply return our function within the first then() method,
// Since the first then() method now returns a function, we can tack on a .then() method to the end of the first then().
// The catch method will catch ALL errors for ALL the then() methods, so we don't need to add a seperate catch() for
// every then().
getTodos("./todos/luiji.json")
  .then(data => {
    console.log("Promise 1 Resolved: ", data);
    return getTodos("./todos/mario.json");
  })
  .then(data => {
    console.log("Promise 2 Resolved:", data);
    return getTodos("./todos/max.json");
  })
  .then(data => {
    console.log("Promise 3 Resolved:", data);
  })
  .catch(error => {
    console.log("Promise Rejected: ", error);
  });
