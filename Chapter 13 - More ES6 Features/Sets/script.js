// Sets - like arrays without duplicates

const nameArray = ["ryu", "chun-li", "ryu", "shaun"];
console.log(nameArray);

const nameSet = new Set(nameArray);
console.log(nameSet);

// Converting set back into array using spread
const uniqueNames = [...nameSet];
console.log(uniqueNames);

// Adding values to sets
const ages = new Set();
ages.add(20);
ages.add(25);
ages.add(30).add(40);

console.log(ages);
// removing items
ages.delete(40);

console.log(ages);

// Size of set
console.log(ages.size);

// Check if value exists in set (bool)
console.log(ages.has(25), ages.has(250));

// Clear a set
ages.clear();
console.log(ages);

// Cycling through sets
const ninjas = new Set([
  { name: "shaun", age: 30 },
  { name: "crystal", age: 29 },
  { name: "chun", age: 32 }
]);

ninjas.forEach(ninja => {
  console.log(ninja.name, ninja.age);
});
