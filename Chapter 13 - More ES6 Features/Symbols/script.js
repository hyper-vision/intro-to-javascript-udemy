// Symbols are a kind of dtype in JS
// No two symbols can be the same

const symbolOne = Symbol("name");
const symbolTwo = Symbol("name");

console.log(symbolOne, symbolTwo, typeof symbolOne);
console.log(symbolOne === symbolTwo);
console.log(symbolOne == symbolTwo);

// Symbols could be used as keys in objects

const ninja = {};

ninja.age = 25;
ninja["belt"] = "orange";
ninja["belt"] = "black";

ninja[symbolOne] = "ryu";
ninja[symbolTwo] = "saun";

console.log(ninja);
