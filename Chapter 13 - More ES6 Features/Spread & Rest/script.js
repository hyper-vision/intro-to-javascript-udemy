// rest parameter (like *args in python)
// When you don't know how many arguments are to be passed
// to the function.
const double = (...nums) => {
  console.log(nums);
  return nums.map(num => num * 2);
};

const result = double(1, 2, 3, 4, 5, 6, 7, 8, 9);
console.log(result);

// spread syntax (arrays)
// Opposite of rest, takes an array and splits it
// into its components
const people = ["red", "ryu", "fox"];
const members = ["mario", "luigi", ...people];
console.log(members);

// spread syntax (objects)
const person = { name: "red", age: 25, job: "net ninja" };
const personClone = { ...person }; // Creates a clone of object person
console.log(personClone);
