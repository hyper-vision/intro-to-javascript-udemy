const ul = document.querySelector(".people");

const people = [
	"Red",
	"Mario",
	"Luigi"
];

let html = ``;

people.forEach((person, i) => {
  html += `<li>${person}</li>`;
});

console.log(html);

ul.innerHTML = html;
