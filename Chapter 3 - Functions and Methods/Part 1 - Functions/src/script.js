// Function Expression (ends in semicolon)
// const speak = function() {
//   console.log("good day");
// };

// Function declaration
// function greet() {
//   console.log("Hello There! I have the high ground!");
// }

// speak();
// greet();

/* 
Hoisting:

When a function is defined using the "Declaration"
method, it is 'hoisted', i.e it is moved to the top
of the program at runtime. This means that no matter
where we declare greet(), we can call it anywhere in
the program.

However, functions defined using the "Expression"
method are NOT 'hoisted'. So they must always be 
defined first before use. 

It's better to use function expressions as they force
you to define the function before calling them, leading
to better code.
*/

// Arguments and Parameters
// const speak = function(name = "Red", time = "night") {
//   console.log(`good ${time} ${name}`);
// };

// // speak("Mario", "morning");
// speak();

// Returning Values
// const calcArea = function(radius) {
//   // let area = 3.14 * radius ** 2;
//   // return area;
//   return 3.14 * radius ** 2;
// };

// const area = calcArea(3);
// console.log(area);

// Arrow Functions
//  regular function
// const calcArea = function(radius) {
//   return 3.14 * radius ** 2;
// };

//  arrow function
//  more succinct - no need for 'function' keyword
// const calcArea = (radius) => {
//   return 3.14 * radius ** 2;
// };

//  further simplification
//  We can get rid of the () if we pass
//  EXACTLY 1 argument.
//  For 0 or 2 or more parameters we need ()
// const calcArea = radius => {
//   return 3.14 * radius ** 2;
// };

//  We can also get rid of the return statement
//  if we only return one element.
// const calcArea = radius => 3.14 * radius ** 2;

// const area = calcArea(3);
// console.log("area is: ", area);

// // Examples of Arrow Functions
// const greet = () => "Hello World";
// console.log(greet());

// const add = (x, y) => x + y;
// console.log(add(1, 2));

// const sumDiff = (x, y) => {
//   let sum = x + y;
//   let diff = x - y;
//   return [sum, diff];
// };

// console.log(sumDiff(1, 2));

// Foreach Method and Callback
// Callback Function - A function we pass in as
// an argument to another function.
// const myFunc = callbackFunction => {
//   let value = 10;
//   callbackFunction(value);
// };

// // Calling myFunc and passing a function as
// // the argument.
// myFunc(value => {
//   console.log(value);
// });

// forEach - a method for iterating over each element in
// an array.

let people = [
  "Peach",
  "Daisy",
  "Rosalina",
  "Bowsette",
  "Zelda",
  "Palutena",
  "Samus"
];

// forEach expects a callback function as an argument
// The first argument accepted by the callback function
// is a variable (we can name it whatever) that is each
// element in the array.

// Like python's: for person in people: etc...

people.forEach(name => {
  console.log(name);
});

// Additionally forEach also accepts a second parameter,
// which is the index of the corresponding value in the
// array

// Like python's: for i, person in enumerate(people): etc...

const logPerson = (name, index) => {
  console.log(`${index} - ${name} is best waifu! `);
};

people.forEach(logPerson);
