const clock = document.querySelector(".clock");

const tick = () => {
  const now = new Date();

  const hours = now.getHours();
  const mins = now.getMinutes();
  const secs = now.getSeconds();

  const html = `<h1><span>${hours}</span> : <span>${mins}</span> : <span>${secs}</span></h1>`;
  clock.innerHTML = html;
};

setInterval(tick, 1000);
