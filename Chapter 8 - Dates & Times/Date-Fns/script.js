// Date-fns is a library that extends the datetime capabilities
// of JS. (Accessed using dateFns object)

const now = new Date();

// console.log(dateFns.isToday(now));

// Formatting options: dateFns.format(now, 'format-string')
console.log(dateFns.format(now, "YYYY"));
console.log(dateFns.format(now, "MMMM"));
console.log(dateFns.format(now, "DD"));
console.log(dateFns.format(now, "Do"));
console.log(dateFns.format(now, "MM/DD/YYYY"));
console.log(dateFns.format(now, "dddd, Do, MMMM, YYYY"));

// Comparing dates
const before = new Date("January 1 2019 12:00:00");
// Getting difference between dates in words
console.log(dateFns.distanceInWords(now, before, { addSuffix: true }));
