// Dates and Times
// Create a new instance of the Date object
// new is used to invoke constructors
// const now = new Date();
// console.log(now);
// console.log(typeof now);

// // years, months, day, times
// console.log("getFullYear", now.getFullYear());
// // jan = 0 - dec = 11
// console.log("getMonth", now.getMonth());
// console.log("getDate", now.getDate());
// // sun = 0, sat = 6
// console.log("getDay", now.getDay());

// console.log("getHours", now.getHours());
// console.log("getMinutes", now.getMinutes());
// console.log("getSeconds", now.getSeconds());

// // Timestamps (No of miliseconds since Jan 1, 1970)
// console.log("timestamp", now.getTime());

// // date strings
// console.log(now.toDateString());
// console.log(now.toTimeString());
// console.log(now.toLocaleString());

// Timestamps (ms since 1 Jan, 1970)
// Useful for comparing times
const before = new Date("January 1 2019, 7:30:59");
const now = new Date();

// console.log(now.getTime(), before.getTime());
const diff = now.getTime() - before.getTime();
console.log(diff);

// Converting milliseconds diff to other formats
// Mins, Hrs, Days

const mins = Math.round(diff / 1000 / 60);
console.log(mins, "Minutes Passed");

const hours = Math.round(mins / 60);
console.log(hours, "Hours Passed");

const days = Math.round(hours / 24);
console.log(days, "Days Passed");

// Converting timestamps into date objects
const timestamp = before;
// First convert timestamp into date object
console.log(new Date(timestamp));
