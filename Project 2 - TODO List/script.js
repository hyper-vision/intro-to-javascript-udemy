const addForm = document.querySelector(".add"); // Input field
const todoList = document.querySelector(".todos"); // list ul
const searchInput = document.querySelector(".search input"); // search field

// Adding items to the list
const generateTemplate = todo => {
  const html = `<li
    class="list-group-item d-flex justify-content-between align-items-center"
  >
    <span>${todo}</span><i class="far fa-trash-alt delete"></i>
  </li>`;
  todoList.innerHTML += html;
};

// Deleting items from the list
todoList.addEventListener("click", event => {
  if (event.target.classList.contains("delete")) {
    event.target.parentElement.remove();
  }
});

addForm.addEventListener("submit", event => {
  event.preventDefault();

  // Get the value from the input field
  const todo = addForm.add.value.trim();
  if (todo.length > 0) {
    generateTemplate(todo);
    addForm.reset(); // Resets all input fields on the form
  }
});

// Match term to todos
const filterTodos = term => {
  // Find all items not matching and assign class filtered
  Array.from(todoList.children)
    .filter(todo => !todo.textContent.toLowerCase().includes(term))
    .forEach(todo => todo.classList.add("filtered"));

  // Find matchin items and REMOVE class filtered
  Array.from(todoList.children)
    .filter(todo => todo.textContent.toLowerCase().includes(term))
    .forEach(todo => todo.classList.remove("filtered"));
};

// Search
searchInput.addEventListener("keyup", event => {
  const term = searchInput.value.trim().toLowerCase();
  filterTodos(term);
});
